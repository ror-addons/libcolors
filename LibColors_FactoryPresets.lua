if not LibColors then LibColors = {} end
local Addon = LibColors

function Addon.ResetFactoryPreset(colorName)
	local FactoryPreset = Addon.DefaultConfiguration[colorName]
	if FactoryPreset then
		Addon.CurrentConfiguration[colorName] = FactoryPreset
	end
end

function Addon.ResetFactoryPresets()
	for k in pairs(Addon.DefaultConfiguration) do
		Addon.ResetFactoryPreset(k)
	end
end

function Addon.GetFactoryPresets()
	local FactoryPresets = 	{
		default = 					{	r = 255,	g = 255,	b = 255		},
		["__BLACK"] =				{	r = 0,		g = 0,		b = 0		},
		["__WHITE"]= 				{	r = 255,	g = 255,	b = 255		},
		["__RED"] = 				{	r = 255,	g = 0,		b = 0		},
		["__GREEN"] = 				{	r = 0,		g = 255,	b = 0		},
		["__BLUE"] = 				{	r = 0,		g = 0,		b = 255		},
		["__YELLOW"] = 				{	r = 255,	g = 255,	b = 0		},
		["__RED_SAT"] = 			{	r = 200,	g = 0,		b = 0		},
		["__GREEN_SAT"] = 			{	r = 0,		g = 150,	b = 0		},
		["__RED_DARK"] = 			{	r = 100,	g = 0,		b = 0		},
		["__YELLOW_DARK"] = 		{	r = 200,	g = 200,	b = 0		},
		["__ORANGE_DARK"] = 		{	r = 200,	g = 100,	b = 0		},
		["__LIGHTGREY"] = 			{	r = 220,	g = 220,	b = 220		},
		["_Mork"] =					{	r = 255,	g = 255,	b = 0		},
		["_Gork"] =					{	r = 255,	g = 0,		b = 0		},
		["archtype-heal"] = 		{	r = 144,	g = 105,	b = 209		},
		["archtype-mdps"] = 		{	r = 255,	g = 65,		b = 63		},
		["archtype-rdps"] = 		{	r = 255,	g = 173,	b = 0		},
		["archtype-tank"] = 		{	r = 190,	g = 150,	b = 95		},
		["buffs-blessing"] = 		{	r = 255,	g = 167,	b = 64		},
		["buffs-buff"] = 			{	r = 50,		g = 127.5,	b = 191.25	},
		["__ORANGE"] = 				{	r = 255,	g = 70,		b = 10		},
		["buffs-hot"] = 			{	r = 94,		g = 215,	b = 127.5	},
		["__PINK_DARK"] = 			{	r = 190,	g = 0,		b = 128		},
		["__ORANGE_PALE"] = 		{	r = 255,	g = 128,	b = 64		},
		["Hot-15"] = 				{	r = 150,	g = 200,	b = 150		},
		["Hot-5"] = 				{	r = 200,	g = 200,	b = 100		},
		["Hot-ExtraHot"] = 			{	r = 200,	g = 150,	b = 150		},
		["Hot-Shield"] = 			{	r = 150,	g = 150,	b = 200		},
		["Hot-Extra"] = 			{	r = 200,	g = 150,	b = 200		},
		["mechanic-lowest"] = 		{	r = 50.25,	g = 213,	b = 0		},
		["mechanic-low"] = 			{	r = 187.37,	g = 165.43,	b = 0		},				
		["mechanic-med"] = 			{	r = 249.5,	g = 205.65,	b = 0		},
		["mechanic-medhigh"] = 		{	r = 255,	g = 115,	b = 0		},
		["mechanic-high"] = 		{	r = 255,	g = 5,		b = 0		},		
		["rangelayer-oor"] = 		{	r = 132,	g = 33,		b = 33		},
		["rangelayer-inrange"] = 	{	r = 255,	g = 255,	b = 255		},
		["rangelayer-inrangel2"] = 	{	r = 255,	g = 255,	b = 0		},
		Life0 = 					{	r = 140,	g = 0,		b = 0		},
		Life10 = 					{	r = 158,	g = 70,		b = 32		},
		Life20 = 					{	r = 150,	g = 90,		b = 34		},
		Life30 = 					{	r = 172,	g = 133,	b = 26		},
		Life40 = 					{	r = 176,	g = 154,	b = 65		},
		Life50 = 					{	r = 168,	g = 180,	b = 73		},
		Life60 = 					{	r = 148,	g = 201,	b = 67		},
		Life70 = 					{	r = 125,	g = 200,	b = 98		},
		Life80 = 					{	r = 25,		g = 201,	b = 128		},
		Life90 = 					{	r = 4,		g = 193,	b = 138		},
		Life100 = 					{	r = 0,		g = 207,	b = 187		},
		["__ANTHRACITE"] = 			{	r = 24,		g = 24,		b = 24		},
		["__ANTHRACITE_DARK"] = 	{	r = 16,		g = 16,		b = 16		},
		["Yak_GreyBlueish"]	= 		{	r = 180,	g = 190,	b = 200		},
		["__TURQUOISE"] = 			{	r = 0,		g = 192,	b = 160		},
		["Yak_BlueSoft"] =			{	r = 120,	g = 140,	b = 220		},
		["Yak_PinkDark"] = 			{	r = 168,	g = 40,		b = 120		},
		["Yak_PinkDarkTwo"] =		{	r = 192,	g = 48,		b = 160		}
	}
	for k,v in pairs(FactoryPresets) do v.name = k end
	return FactoryPresets
end